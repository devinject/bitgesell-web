const InsufficientFoundsError = 'InsufficientFoundsError'
const InvalidSendAddressError = 'InvalidSendAddressError'
const ReadWalletError = 'ReadWalletError'
const InvalidPasswordError = 'InvalidPasswordError'

export const ERROR_NAMES = {
    InsufficientFoundsError,
    InvalidSendAddressError,
    ReadWalletError
}

export class InvalidPassword extends Error {
    constructor(message) {
        super(message)

        this.name = InvalidPasswordError
    }
}

export class ReadWallet extends Error {
    constructor(message) {
        super(message)

        this.name = ReadWalletError
    }
}

export class InsufficientFounds extends Error {
    constructor(message) {
        super(message)

        this.name = InsufficientFoundsError
    }
}

export class InvalidSendAddress extends Error {
    constructor(message) {
        super(message)

        this.name = InvalidSendAddressError
    }
}
