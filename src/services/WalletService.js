// const fs = require('fs')
// const path = require('path')

// TODO Fix
/* eslint-disable */
import CryptoJS from 'crypto-js'
import {ReadWallet} from './Errors'


const ENCRYPT_WORD = 'devinject'

export default {
    getLocalWallets() {

        if (!localStorage.wallets) {
            return [];
        }
        return Object.keys(JSON.parse(localStorage.wallets))
    },

    validateWallet(walletData) {
        if (!walletData.mnemonic || !walletData.passphrase || !walletData.addressesCount || !walletData.addressesLabels) {
            throw new ReadWallet()
        }
    },

    openLocalWallet(name) {
        let wallets = JSON.parse(localStorage.wallets)

        console.log(CryptoJS.AES.decrypt(wallets[name], ENCRYPT_WORD).toString(CryptoJS.enc.Utf8))
        return JSON.parse(CryptoJS.AES.decrypt(wallets[name], ENCRYPT_WORD).toString(CryptoJS.enc.Utf8))
    },

    deleteWallet(name) {
        let wallets = JSON.parse(localStorage.wallets)
        delete wallets[name];
        localStorage.wallets = JSON.stringify(wallets)
    },
    renameWallet(name, newName) {
        let wallets = JSON.parse(localStorage.wallets)
        wallets[newName] = wallets[name];
        delete wallets[name];
        localStorage.wallets = JSON.stringify(wallets)
    },

    async openRemoteWallet(filePath, name) {
        let data =  (await new Promise((resolve, reject) => {
            let fr = new FileReader();
            fr.onload = x=> resolve(fr.result);
            fr.readAsText(filePath);
        }))
        let walletData = JSON.parse(CryptoJS.AES.decrypt(data, ENCRYPT_WORD).toString(CryptoJS.enc.Utf8))
        this.validateWallet(walletData)


        let wallets = JSON.parse(localStorage.wallets)
        wallets[name] = data;

        localStorage.wallets = JSON.stringify(wallets)

        return walletData;
    },

    buildWalletFromStore(store) {
        return CryptoJS.AES.encrypt(JSON.stringify({
            mnemonic: store.state.Wallet.seed,
            passphrase: store.state.Wallet.passphraseEncrypt,
            addressesCount: store.state.Wallet.addressesCount,
            addressesLabels: store.state.Wallet.addressesLabels,
        }), ENCRYPT_WORD).toString()
    },

    readToStoreFromWalletData(store, jsonWallet) {
        store.commit('Wallet/seed', jsonWallet.mnemonic)
        store.commit('Wallet/passphraseEncrypt', jsonWallet.passphrase)
        store.commit('Wallet/addressesCount', jsonWallet.addressesCount)
        store.commit('Wallet/addressesLabels', jsonWallet.addressesLabels)
    },

    dumpWallet(mnemonicEncrypt, passphraseEncrypt) {
        return {
            seed: mnemonicEncrypt,
            phrase: passphraseEncrypt
        }
    },

    encrypt(mnemonic, passphrase, password) {
        return {
            seed: CryptoJS.AES.encrypt(mnemonic, password).toString(),
            passphraseEncrypt: CryptoJS.AES.encrypt(passphrase, password).toString()
        }
    },

    decrypt(seed, passphraseEncrypt, password) {
        return {
            mnemonic: CryptoJS.AES.decrypt(seed, password).toString(CryptoJS.enc.Utf8),
            passphrase: CryptoJS.AES.decrypt(passphraseEncrypt, password).toString(CryptoJS.enc.Utf8)
        }
    },

    validateSeed(words) {
        return window.isMnemonicCheckSumValid(words) && window.isMnemonicValid(words)
    },

    /////////////////

    readWallet(filePath) {
        let file = fs.readFileSync(filePath),
            decrypted = CryptoJS.AES.decrypt(file.toString(), ENCRYPT_WORD).toString(CryptoJS.enc.Utf8),
            walletData = JSON.parse(decrypted)


        this.validateWallet(walletData)

        walletData.filePath = filePath

        return walletData
    },

    saveWallet(store) {
        let json = this.buildWalletFromStore(store)
        if (localStorage.wallets) {
            var wallets = JSON.parse(localStorage.wallets)
        } else {
            var wallets = {}
        }
        wallets[store.state.Wallet.walletName] = json;
        localStorage.wallets = JSON.stringify(wallets)
    },

    download(store) {
        let json = this.buildWalletFromStore(store)

        let blob = new Blob([json], {type: 'text/plain'}),
            url = window.URL.createObjectURL(blob),
            a = document.createElement('a')

        document.body.appendChild(a)

        let name = store.state.Wallet.walletName ? store.state.Wallet.walletName : 'wallet'

        a.href = url
        a.download = name + '.di'
        a.style = 'display: none'

        a.click()

        window.URL.revokeObjectURL(url)
        document.body.removeChild(a)
    }
}
