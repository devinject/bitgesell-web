import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/index'
import WalletService from "../services/WalletService";

// TODO Fix
/* eslint-disable */

Vue.use(Router)

let middleware = (to, from, next) => {

    let setupsWithoutName = [
        '/setup/access-to-wallet',
        '/setup/keystore',
        '/setup/generate-seed',
        '/setup/insert-seed',
        '/setup/password',
        '/setup/extension-seed',
    ];

    console.log('---------------');


    if ((to.path === from.path && to.path === '/setup/access-to-wallet') ||
        (to.path.match(/^\/main/) && !store.state.Wallet.wallet && from.path === '/setup/access-to-wallet')) {
        next('/setup/wallet')
        return
    }

    localStorage.lastFullUrlIsAccess = from.path === '/setup/access-to-wallet';

    if (to.path.match(/^\/main/) && !store.state.Wallet.wallet) {
        if (localStorage.lastWallet) {
            store.commit('Wallet/walletName', localStorage.lastWallet);
            localStorage.lastUrl = to.path;

            let wallet = WalletService.openLocalWallet(localStorage.lastWallet)
            WalletService.readToStoreFromWalletData(store, wallet)
            localStorage.lastFullUrl = '/setup/access-to-wallet';
            next('/setup/access-to-wallet');

        } else {
            if (localStorage.lastUrl) {
                delete localStorage.lastUrl;
            }
            next('/setup/wallet')
        }
    } else if (to.path === '/setup/access-to-wallet' && (from.path === '/' || from.path === '/setup/wallet') && !store.state.Wallet.walletName) {
        next('/setup/wallet')
    }
    else if (setupsWithoutName.indexOf(to.path) >= 0 && !store.state.Wallet.walletName) {
        next('/setup/wallet')
    }
    else {
        next()

    }

}


const router = new Router({

    base: "/app/",
    mode: 'history',
    routes: [
        {
            path: '/',
            component: require('@/components/Setup/Description').default,
            meta: {
                title: 'Bitgesell wallet'
            }
        },

        {
            path: '/setup/wallet',
            component: require('@/components/Setup/Wallet').default
        },

        {
            path: '/setup/access-to-wallet',
            component: require('@/components/Setup/AccessToWallet').default
        },

        {
            path: '/setup/keystore',
            component: require('@/components/Setup/Keystore').default
        },

        {
            path: '/setup/generate-seed',
            component: require('@/components/Setup/GenerateSeed').default
        },

        {
            path: '/setup/extension-seed',
            component: require('@/components/Setup/ExtensionSeed').default
        },

        {
            path: '/setup/insert-seed',
            component: require('@/components/Setup/InsertSeed').default
        },

        {
            path: '/setup/password',
            component: require('@/components/Setup/Password').default
        },

        ///////////////

        {
            path: '/main',
            component: require('@/components/Main/History').default,
        },

        {
            path: '/main/send',
            component: require('@/components/Main/Send').default,
        },

        {
            path: '/main/receive',
            component: require('@/components/Main/Receive').default,
        },

        {
            path: '/main/address',
            component: require('@/components/Main/Address').default,
        },

        {
            path: '/main/change-password',
            component: require('@/components/Main/ChangePassword').default,
        },

        {
            path: '/main/get-seed',
            component: require('@/components/Main/GetSeed').default,
        },

        ///////////////

        {
            path: '*',
            redirect: '/'
        }
    ]
})
router.beforeEach(middleware)

const DEFAULT_TITLE = 'Bitgesell wallet';
router.afterEach((to, from) => {
    Vue.nextTick(() => {
        document.title = to.meta.title || DEFAULT_TITLE;
    });
});

export default router