import Vue from 'vue'
import Vuex from 'vuex'

import modules from './modules'

Vue.use(Vuex)

Vue.config.devtools = process.env.NODE_ENV === 'development'
const store = new Vuex.Store({
    modules,

    strict: process.env.NODE_ENV !== 'production'
})

export default store
