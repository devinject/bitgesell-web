import axios from 'axios'

import BalanceService from '../../services/BalanceService'

import {SERVER_URL} from '../../configs'

const state = {
    transactionsInfo: [],
    minFee: 100,
    defaultFeeVbyte: 2
}

const getters = {
    utxo: (state, _, rootState, rootGetters) => {
        let utxo = [],
            addresses = rootGetters['Wallet/addresses'].map(x => x.address),

            info = state.transactionsInfo

        for (let transactionInfo of state.transactionsInfo) {
            for (let vout of transactionInfo.to) {
                if (!vout.used && transactionInfo.confirmations && addresses.find(x => x === vout.address)) {
                    utxo.push(vout)
                }
            }
        }

        return utxo
    },

    transactions: (state, _, rootState, rootGetters) => {
        let addresses = rootGetters['Wallet/addresses'].map(x => x.address)

        return state.transactionsInfo.map((transaction) => {
            let amount = 0,
                send = 0

            // TODO: split if one address is ours and the other is not
            for (let address of transaction.from) {
                if (addresses.find(x => x === address)) {
                    send = transaction.amount
                }
            }

            let come = 0

            for (let vout of transaction.to) {
                if (addresses.find(x => x === vout.address)) {
                    come += vout.amount
                }
            }

            return {
                id: transaction.transaction_id,
                amount: Math.abs(come - send),
                date: transaction.date,
                confirmations: transaction.confirmations,
                status: [
                    ((come - send) > 0 ? 'plus' : 'minus')
                ]
            }
        })
    },

    balance: (state, _, rootState, rootGetters) => {
        let sum = 0,
            addresses = rootGetters['Wallet/addresses'].map(x => x.address),
            info = state.transactionsInfo

        for (let transaction of info) {
            for (let vout of transaction.to) {
                if (!vout.used && transaction.confirmations && addresses.find(x => x === vout.address)) {
                    sum += vout.amount
                }
            }
        }

        return sum
    },

    balanceLocked: (state, _, rootState, rootGetters) => {
        let sum = 0,
            addresses = rootGetters['Wallet/addresses'].map(x => x.address),
            info = state.transactionsInfo

        for (let transaction of info) {
            for (let vout of transaction.to) {
                if (!vout.used && !transaction.confirmations && addresses.find(x => x === vout.address)) {
                    sum += vout.amount
                }
            }
        }

        return sum
    },

    addressBalances: (state, _, rootState, rootGetters) => {
        let balances = {},
            addresses = rootGetters['Wallet/addresses'].map(x => x.address),
            info = state.transactionsInfo

        for (let transaction of info) {
            for (let vout of transaction.to) {
                if (!vout.used && transaction.confirmations && addresses.find(x => x === vout.address)) {
                    balances[vout.address] = vout.amount
                }
            }
        }

        for (let address of addresses) {
            balances[address] = balances[address] ? balances[address] : 0
        }

        return balances
    }
}

let setters = {}

for (let i in state) {
    setters[i] = (state, value) => {
        state[i] = value
    }
}

const mutations = {
    ...setters
}

const actions = {
    async loadTransactionsInfo(store) {
        let transactionsInfo = await BalanceService.getTransactions(store.rootGetters['Wallet/addresses'])

        store.commit('transactionsInfo', transactionsInfo)
    },

    async loadMinInfo(store) {
        let data = (await axios.get(SERVER_URL + 'site/get-min-info')).data

        store.commit('minFee', data.minFee)
        store.commit('defaultFeeVbyte', data.defaultFeeVbyte)
    }
}

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
