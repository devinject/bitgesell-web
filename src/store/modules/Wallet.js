import CryptoJS from 'crypto-js'

import WalletService from '@/services/WalletService'
import {InvalidPassword} from "../../services/Errors";

const state = {
    walletName: '',
    walletMnemonic: '',
    wallet: null,
    filePath: null,
    seed: '',
    passphrase: '',
    passphraseEncrypt: '',

    addressesCount: 20,
    addressesLabels: {},
}

const actions = {
    unusedAddress(store, label) {
        let i = 0

        while (true) {
            if (!state.addressesLabels[state.wallet.getAddress(i).address]) {
                if (!this.getters['Balance/addressBalances'][state.wallet.getAddress(i).address]) {
                    break
                }
            }

            i++
        }

        if (i > state.addressesCount) {
            state.addressesCount = i + 1
        }

        let address = store.state.wallet.getAddress(i).address;

        store.commit('useAddress', {
            address,
            label
        })

        return address
    }
}

const getters = {
    addresses(state) {
        let addresses = []

        for (let i = 0; i < state.addressesCount; i++) {
            addresses.push(state.wallet.getAddress(i))
        }

        return addresses
    }
}

let setters = {}

for (let i in state) {
    setters[i] = (state, value) => {
        state[i] = value
    }
}

const mutations = {
    ...setters,

    useAddress(state, {address, label}) {
        state.addressesLabels[address] = {
            label: label
        }

        WalletService.saveWallet(this)
    },

    generateMnemonic(state) {
        state.walletMnemonic = (new Wallet({strength: 128})).mnemonic
    },

    decryptWallet(state, password) {
        let {mnemonic, passphrase} = WalletService.decrypt(state.seed, state.passphraseEncrypt, password)
        if (state.wallet.mnemonic !== mnemonic) {
            throw new InvalidPassword()

        }
        if (state.wallet.passphrase !== passphrase) {
            throw new InvalidPassword()
        }
    },

    setWalletBySeedAndPassword(state, password) {
        let {mnemonic, passphrase} = WalletService.decrypt(state.seed, state.passphraseEncrypt, password)

        state.wallet = new Wallet({
            from: mnemonic,
            passphrase: passphrase
        })

        state.passphrase = ''
        state.walletMnemonic = ''
    },

    setWalletByMnemonicAndPassword(state, password) {
        state.wallet = new Wallet({
            from: state.walletMnemonic,
            passphrase: state.passphrase
        })

        let {passphraseEncrypt, seed} = WalletService.encrypt(state.wallet.mnemonic, state.wallet.passphrase, password)

        state.passphraseEncrypt = passphraseEncrypt
        state.seed = seed

        WalletService.saveWallet(this)

        state.passphrase = ''
        state.walletMnemonic = ''
    },

    setWalletByMnemonicAndPasswordOldWallet(state, password) {
        let {passphraseEncrypt, seed} = WalletService.encrypt(state.wallet.mnemonic, state.wallet.passphrase, password)

        state.passphraseEncrypt = passphraseEncrypt
        state.seed = seed

        WalletService.saveWallet(this)

        state.passphrase = ''
        state.walletMnemonic = ''
    },

    clear(state) {
        state.walletName = ''
        state.walletMnemonic = ''
        state.wallet = null
        state.filePath = ''
        state.seed = ''
        state.passphrase = ''
        state.passphraseEncrypt = ''
    }
}

export default {
    namespaced: true,
    getters,
    state,
    mutations,
    actions
}
