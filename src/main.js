import './assets/css/normalize.css'
import './assets/css/grid.css'
import './assets/scss/default.scss'

import Vue from 'vue'
import App from './components/App'

import router from './router'
import store from './store'

import axios from 'axios'
import i18n from './i18n'

import NotificationPlugin from './plugins/NotificationPlugin'
import './registerServiceWorker'

//////////////////////////////

(async function () {
    const jsbtc = require('./helpers/bgl/jsbgl')
    await jsbtc.asyncInit(window)

    Vue.http = Vue.prototype.$http = axios
    Vue.config.productionTip = false

    Vue.filter('number', str => Number(
        str.toFixed(8)
    ))

    Vue.use(NotificationPlugin)

    new Vue({
        ...App,
        router,
        store,
        i18n
    })
}())
