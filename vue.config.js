module.exports = {
	publicPath: '/app/',
	chainWebpack: config => {
		config
			.plugin('html')
			.tap(args => {
				args[0].title = 'Bitgesell wallet'
				return args
			})
	},

	pwa: {
		name: 'BGL Wallet',
		themeColor: '#ffffff',
		msTileColor: '#000000',
		appleMobileWebAppCapable: 'yes',
		appleMobileWebAppStatusBarStyle: 'white',

		iconPaths: {

			favicon32: 'img/icons/32x32.png',
			favicon16: 'img/icons/16x16.png',
			appleTouchIcon: 'img/icons/152x152.png',
			maskIcon: 'img/icons/icon.svg',
			msTileImage: 'img/icons/144x144.png'
		},

		manifestOptions: {
			display: 'landscape',
			background_color: '#ffffff',
			icons: [
				{ 'src': './img/icons/192x192.png', 'sizes': '192x192', 'type': 'image/png' },
				{ 'src': './img/icons/512x512.png', 'sizes': '512x512', 'type': 'image/png' },
				{ 'src': './img/icons/192x192.png', 'sizes': '192x192', 'type': 'image/png', 'purpose': 'maskable' },
				{ 'src': './img/icons/512x512.png', 'sizes': '512x512', 'type': 'image/png', 'purpose': 'maskable' }
			]
		},


	}
}